import {CommandComponent} from '../command.component';
import {IngredientComponent} from '../ingredient.component';
import {MilkComponent} from '../ingredient/milk.component';
import {SugarComponent} from '../ingredient/sugar.component';

export class IngredientFactoryComponent {
  static create(name: string = '') {
    let ingredient;

    switch (name) {
      case 'milk':
        ingredient = new MilkComponent();
        break;
      case 'sugar':
        ingredient = new SugarComponent();
        break;
    }

    return ingredient;
  }

  static createRandom(numberOfElmt: number) {
    const commands = [];

    for (let i = 0; i < numberOfElmt; i++) {
      commands.push(IngredientFactoryComponent.create());
    }

    return commands;
  }
}
