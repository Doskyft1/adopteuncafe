import {CommandComponent} from '../command.component';

export class CommandFactoryComponent {
  static create() {
    return new CommandComponent();
  }

  static createRandom(numberOfElmt: number) {
    const commands = [];

    for (let i = 0; i < numberOfElmt; i++) {
      commands.push(CommandFactoryComponent.create());
    }

    return commands;
  }
}
