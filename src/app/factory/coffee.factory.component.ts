import {DecaComponent} from '../coffee/deca.component';
import {ArabicaComponent} from '../coffee/arabica.component';
import {IngredientFactoryComponent} from './ingredient.factory.component';

export class CoffeeFactoryComponent {
  static create(type: string = 'deca', ingredients: string[] = []) {
    let coffee;
    const ingredientsObj = [];

    ingredients.forEach(ingredient => {
      ingredientsObj.push(IngredientFactoryComponent.create(ingredient));
    });

    switch (type) {
      case 'deca':
        coffee = new DecaComponent(ingredientsObj);
        break;
      case 'arabica':
        coffee = new ArabicaComponent(ingredientsObj);
        break;
    }

    return coffee;
  }

  static createRandom(numberOfElmt: number) {
    const coffeeList = ['deca', 'arabica'];
    const ingredientList = ['milk', 'sugar'];
    const coffees = [];

    for (let i = 0; i < numberOfElmt; i++) {
      const ingredients = [];
      for (let j = 0; j < Math.floor(Math.random() * Math.floor(10)); j++) {
        ingredients.push(ingredientList[Math.floor(Math.random() * Math.floor(ingredientList.length))]);
      }

      coffees.push(CoffeeFactoryComponent.create(coffeeList[Math.floor(Math.random() * Math.floor(coffeeList.length))], ingredients));
    }

    return coffees;
  }
}
