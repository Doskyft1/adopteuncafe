import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {CommandFactoryComponent} from './factory/command.factory.component';
import {CommandComponent} from './command.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnChanges, OnInit {
  title = 'My Coffee Shop';
  commands: CommandComponent[] = [];
  @Output() products$: Observable<number>;

  constructor(private changeDetector: ChangeDetectorRef) {
    console.log('command const');
  }

  ngOnInit() {
    this.products$.subscribe( {
        complete: () => console.log(this),
        next: () => this.changeDetector.markForCheck(),
        error: () => console.error(this)
      });
  }

  addCommand() {
    this.commands.push(CommandFactoryComponent.create());
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('APP CHANGE');
    console.log(changes);
  }
}
