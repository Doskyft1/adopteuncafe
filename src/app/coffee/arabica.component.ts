import {CoffeeComponent} from '../coffee.component';
import {IngredientComponent} from '../ingredient.component';

export class ArabicaComponent extends CoffeeComponent {
  ingredients: IngredientComponent[];

  constructor(ingredients: IngredientComponent[]) {
    super(ingredients);
    this.description = 'Arabica';
    this.price = 2;
  }
}
