import {CoffeeComponent} from '../coffee.component';
import {IngredientComponent} from '../ingredient.component';

export class DecaComponent extends CoffeeComponent {
  ingredients: IngredientComponent[];

  constructor(ingredients: IngredientComponent[]) {
    super(ingredients);
    this.description = 'Deca';
    this.price = 1;
  }
}
