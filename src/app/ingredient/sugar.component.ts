import {IngredientComponent} from '../ingredient.component';

export class SugarComponent extends IngredientComponent {
  name = 'Sucre';
  price = 0.5;
}
