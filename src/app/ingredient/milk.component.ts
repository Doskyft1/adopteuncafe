import {IngredientComponent} from '../ingredient.component';

export class MilkComponent extends IngredientComponent {
  name = 'Lait';
  price = 1.3;
}
