import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import { CoffeeComponent } from './coffee.component';
import { CoffeeFactoryComponent } from './factory/coffee.factory.component';

@Component({
  selector: 'app-command',
  templateUrl: './command.component.html',
})
export class CommandComponent implements OnInit, OnChanges {
  private coffees: CoffeeComponent[] = [];
  @Output() commandChange = new EventEmitter<any>();

  ngOnInit(): void { }

  constructor() {
    console.log('Command const');
    this.commandChange.emit('onChangeEmit');

  }

  public getCoffees(): CoffeeComponent[] {
    return this.coffees;
  }

  public addCoffee($event: string) {
    this.coffees.push(CoffeeFactoryComponent.create($event));
  }

  public totalPrice() {
    let total = 0;
    this.coffees.forEach(coffee => {
      total += coffee.totalPrice();
    });
    return Number(total.toFixed(2));
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('Command');
    console.log(changes);
  }
}
