export class IngredientComponent {
  protected name = '';
  protected price = 0;

  public getName() {
    return this.name;
  }

  public getPrice() {
    return this.price;
  }
}
