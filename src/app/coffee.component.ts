import {Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {IngredientComponent} from './ingredient.component';
import {IngredientFactoryComponent} from './factory/ingredient.factory.component';

export class CoffeeComponent implements OnInit, OnChanges {

  protected description = '';
  protected price = 0;
  protected ingredients: IngredientComponent[];

  constructor(ingredients: IngredientComponent[]) {
    this.ingredients = ingredients;
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  public getDescription() {
    return this.description;
  }

  public totalPrice() {
    let price = this.price;
    this.ingredients.forEach(ingredient => {
      price += ingredient.getPrice();
    });
    return Number(price.toFixed(2));
  }

  public getPrice() {
    return this.price;
  }

  public getIngredient() {
    return this.ingredients;
  }

  public addIngredient($event: string) {
    this.ingredients.push(IngredientFactoryComponent.create($event));
  }
}
